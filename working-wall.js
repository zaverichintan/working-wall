Pictures = new Mongo.Collection("pictures");

Pictures.allow({
  insert: function(userId, doc) {
      return true;
  }
});

if (Meteor.isClient) {

  Meteor.subscribe("pictures");

  Template.pictures.helpers({
    pictures: function () {
      return Pictures.find({}, {sort: {'date_added': -1}, limit: 30}).fetch();
    }
  });

  Template.pictures.rendered = function() {
    var video = document.querySelector('video');
    var canvas = document.querySelector('canvas');
    var ctx = canvas.getContext('2d');
    var localMediaStream = null;
    GAnalytics.pageview();

    function snapshot() {
      if (localMediaStream) {
        ctx.drawImage(video, 0, 0, 400, 150);
        Pictures.insert({url: canvas.toDataURL('image/png'), date_added: new Date});
        video.src = window.URL.createObjectURL(localMediaStream);
        GAnalytics.event("picture","taken");
      }
    }

    function sorry() {
      var video_holder = document.getElementsByClassName('main-picture');
      video_holder[0].innerHTML = '<img src="/sorry.png"/>';
      GAnalytics.event("picture","error");
    }

    video.addEventListener('click', snapshot, false);

    navigator.getUserMedia  = navigator.getUserMedia ||
                              navigator.webkitGetUserMedia ||
                              navigator.mozGetUserMedia ||
                              navigator.msGetUserMedia;

    if (navigator.getUserMedia) {
      navigator.getUserMedia({video: true}, function(stream) {
        video.src = window.URL.createObjectURL(stream);
        localMediaStream = stream;
      }, sorry);
    } else {
      sorry();
    }
  }
}

if (Meteor.isServer) {
  Meteor.startup(function () {});
  Meteor.publish("pictures", function() {
    return Pictures.find({}, {sort: {'date_added': -1}, limit: 60});
  });
}
